log_level                :info
log_location             STDOUT
node_name                "#{ENV['USER']}"
client_key               ENV['KNIFE_CLIENT_KEY']
validation_client_name   'chef-validator'
validation_key           ENV['KNIFE_VALIDATION_KEY']
chef_server_url          'https://cookie.cloud.caltech.edu:443'
syntax_check_cache_path  ENV['KNIFE_SYNTAX_CHECK_CACHE']
ssl_verify_mode          :verify_none

knife[:aws_access_key_id] = ENV['AWS_ACCESS_KEY']
knife[:aws_secret_access_key] = ENV['AWS_SECRET_KEY']
knife[:aws_ssh_key_id] = ENV['AWS_SSH_KEY_ID']
knife[:region] = 'us-west-1'
knife[:identity_file] = "#{ENV['HOME']}/.ssh/glennkey2.pem"