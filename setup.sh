#!/bin/bash

SOURCE_DIR=`pwd`

echo "*****"
echo "Work Machine Bootstrap (setup phase)"
echo "*****"

echo "**"

echo Generating SSH key pair, be prepared to choose a password for the keys.
cd ~
mkdir ~/.ssh
chmod 700 ~/.ssh
cp ${SOURCE_DIR}/templates/ssl_config ~/.ssh/config
ssh-keygen -b 2048 -t rsa -f id_rsa

echo "**"

echo Configuring syslog
sudo cp ${SOURCE_DIR}/templates/asl.conf /etc/asl.conf

echo "**"

echo Installing and configuring kitchenplan
sudo gem install kitchenplan
kitchenplan setup --gitrepo=https://github.com/smeinel/kitchenplan-config.git

if [[ -f /opt/kitchenplan/config/people/${USER}.yml ]];
then
	echo "User template in repo!"
else
	echo Creating basic dev user template
	cat ${SOURCE_DIR}/templates/user.yml | perl -p -e "s#USER#${USER}#g" > /opt/kitchenplan/config/people/${USER}.yml
fi

echo "**"

echo Running kitchenplan
kitchenplan provision

echo ""
echo "*****"
echo "Setup is complete. At this point, you need to:"
echo "  * Open a new terminal window"
echo "  * cd to work_machine_bootstrap repo"
echo "  * run './post_setup.sh'"
