#!/bin/bash

SOURCE_DIR=`pwd`
TEMPLATE_DIR=${SOURCE_DIR}/templates

echo "*****"
echo "Work Machine Bootstrap (post-setup phase)"
echo "**"
echo "Setting basic shell environment"
cd ~
cp ${TEMPLATE_DIR}/dot_bash_profile ~/.bash_profile
cp ${TEMPLATE_DIR}/dot_env ~/.env
cp ${TEMPLATE_DIR}/dot_pyenv_init ~/.pyenv_init

mkdir -p .chef
cp ${TEMPLATE_DIR}/knife.rb ~/.chef/knife.rb

echo "**"
echo "Using new shell environment"
source ~/.bash_profile

echo "**"
echo "Installing pyenv python 2.7.8"
pyenv install 2.7.8
pyenv global 2.7.8
pyenv virtualenv 2.7.8 ve-2.7.8

echo "**"
echo "Install rsync (kitchenplan wasn't able to for some odd reason"
brew install rsync

echo ""
echo "*****"
echo "Post setup is complete. At this point, you need to:"
echo "  * Obtain your AWS keys and update them in ~/.env"
echo "  * Obtain your Chef keys and place them in ~/.ssh"
