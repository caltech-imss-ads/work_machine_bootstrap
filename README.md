# Work Machine Bootstrap

This set of shell scripts is intended to take you from a new OS X install to having a working development machine.

This set of scripts will do the following:

* Generate a SSH key pair
* Create `~/.ssh/config`
* Update `/etc/asl.conf` to log at `info` level
* Install & configure `kitchenplan`
* Run `kitchenplan`
* Set up an intial `pyenv virtualenv`
* Update `.bash_profile`
* Display instructions for manual portions of the bootstrap. This set of instructions should hopefully be minimal. Get AWS Keys, get Chef keys, etc.

## Usage

The first time you use `git`, you'll be prompted to install the OS X Developer Tools. Then, you'll need to perform the clone since the first invocation was aborted.

```
git clone https://bitbucket.org/caltech-imss-ads/work_machine_bootstrap.git
cd work_machine_bootstrap
./setup.sh
```

After `setup.sh` completes, open a new `Terminal` window. In that window, invoke:

```
cd work_machine_bootstrap
./post_setup.sh
```